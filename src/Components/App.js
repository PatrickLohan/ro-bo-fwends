import React, { Component } from "react";
import CardArray from "./CardArray";
import SearchBox from "./SearchBox";
import { robots } from "./Robots.js";

class App extends Component {
  constructor() {
    super();
    this.state = {
      robots: robots,
      searchfield: ""
    };
  }

  onSearchChange = event => {
    this.setState({
      searchfield: event.target.value
    });
  };

  render() {
    const filteredRobots = this.state.robots.filter(robot => {
      return robot.name
        .toLowerCase()
        .includes(this.state.searchfield.toLowerCase());
    });

    return (
      <div className="tc">
        <h1>Robofriends</h1>
        <SearchBox searchChange={this.onSearchChange} />
        <CardArray robots={filteredRobots} />
      </div>
    );
  }
}

export default App;
